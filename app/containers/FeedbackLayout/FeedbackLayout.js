import React from 'react';
import { Helmet } from 'react-helmet';
import Animations from '../../components/Animations';
import './style.scss';

export default class FeedbackLayout extends React.Component {
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <div className="feedback-page">
        <h1>Feedback Page</h1>
        <div align="center">
          <Animations/>
        </div>
      </div>
    );
  }
}
