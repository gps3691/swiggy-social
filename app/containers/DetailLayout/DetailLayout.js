import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import './style.scss';
import CombinedOrderState from './Partials/CombinedOrderState/CombinedOrderState.js';
import MenuItemCard from './Partials/MenuItemCard/MenuItemCard'

export default class DetailLayout extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      menuData: [],
      cartData: {},
      totalItems: 0
    }  

    this.addToCart = this.addToCart.bind(this);
    this.removeFromCart = this.removeFromCart.bind(this);
  }

  componentDidMount(){
    this.props.loadMenuData();
  }

  componentWillReceiveProps(data){
      this.setState({menuData: data.menuData})       
  }

  addToCart(value){
    value.count = 1;
    this.setState((prevState, props) => ({
      totalItems: prevState.totalItems + 1
    }));

    fetch('http://localhost:3000/api/addItem', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(value)
    }).then(res => res.json())
    .then(response => {
      let groupOrderCount = 0;
      response.cart.forEach((item) =>{
        groupOrderCount += item.groupCount
      })
      
      this.setState({
        cartData : response,
        totalItems : groupOrderCount
      })

      // console.log('Success:', JSON.stringify(response))
    })
    .catch(error => {
      console.error('Error:', error)
    });

  }

  removeFromCart(value){
    value.count = -1;
    this.setState((prevState, props) => ({
      totalItems: prevState.totalItems - 1
    }));

    fetch('http://localhost:3000/api/addItem', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(value)
    }).then(res => res.json())
    .then(response => {
      let groupOrderCount = 0;
      response.cart.forEach((item) =>{
        groupOrderCount += item.groupCount
      })
      
      this.setState({
        cartData : response,
        totalItems : groupOrderCount
      })
      
      // console.log('Success:', JSON.stringify(response))
    })
    .catch(error => {
      console.error('Error:', error)
    });
  }

  render() {
    const { loading, error} = this.props;
    let {menuData} = this.state;

    return (
      <div className="detail-page">
        <h1>Details Page</h1>
          <div className="detail-page-body">
            {this.state.menuData.map(hit =>
                <MenuItemCard key={hit.id} cardData={hit} handleAddToCart={this.addToCart} handleRemoveFromCart={this.removeFromCart}/>
            )}
            <CombinedOrderState itemsCount={this.state.totalItems} cartData={this.state.cartData.cart}/>
          </div>
      </div>
    );
  }
}

DetailLayout.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  menuData: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
  ]),
};


