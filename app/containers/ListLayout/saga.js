import { call, put, select, takeLatest } from 'redux-saga/effects';
import { LOAD_LIST } from 'containers/ListLayout/constants';
import { listLoaded, listLoadingError } from 'containers/ListLayout/actions';
import request from 'utils/request';

export function* getList() {
  const requestURL = `http://demo8213882.mockable.io/fetchResto`;
  try {
    const resp = yield call(request, requestURL);
    yield put(listLoaded(resp));
  } catch (err) {
    yield put(listLoadingError(err));
  }
}

export default function* listData() {
  yield takeLatest(LOAD_LIST,  getList);
}
