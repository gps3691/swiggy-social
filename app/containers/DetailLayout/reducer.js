/*
 * ListReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  LOAD_MENU_SUCCESS,
  LOAD_MENU,
  LOAD_MENU_ERROR,
} from './constants';

// The initial state of the ListPage
const initialState = fromJS({
  loading: false,
  error: false,
  menuData: []
});

function menuReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_MENU:
      return state
        .set('loading', true)
        .set('error', false)
        .setIn('menuData', []);
    case LOAD_MENU_SUCCESS:
      return state
        .setIn('menuData', action.menu.data)
        .set('loading', false)
    case LOAD_MENU_ERROR:
      return state
        .set('error', action.error)
        .set('loading', false);
    default:
      return state;
  }
}

export default menuReducer;