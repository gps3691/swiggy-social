import React from 'react';
import './style.scss';

export default function NotFound() {
  return (
    <article>
    	<div className="not-found-page">
      		<h1>Page not found.</h1>
  		</div>
    </article>
  );
}
