import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

export default class Card extends React.Component { 
  constructor(props){
    super(...props);

    this.state ={
      selected: false
    }

    this.handleSelection = this.handleSelection.bind(this);
  }

  handleSelection(){
    this.setState({
      selected: !this.state.selected
    })
  }

  render() {
    const { cardData, handleMethod } = this.props;
    return (
          <div className={"Card " + (this.state.selected?"selected":"")} onClick={handleMethod}>
            <input id="restoId" type="hidden" value={cardData.id}/>
            <div className="CardImage">
              <img src={cardData.image} height="150" width="140"/>
            </div>
            <div className="CardTitle">
              {cardData.name}
            </div>
            <div className="CardSubtitle">
              <p>{cardData.cusines}</p>
            </div>
            <div className="FooterWrapper">
              <div className="CardRating">
                &#x2605; {cardData.rating}
              </div>
              <div className="CardDelivery">
                {cardData.deliveryTime} Mins
              </div>
            </div>
          </div>
    );
  }
}

Card.propTypes = {
  cardData: PropTypes.oneOfType([
    PropTypes.object,
  ]),
};
