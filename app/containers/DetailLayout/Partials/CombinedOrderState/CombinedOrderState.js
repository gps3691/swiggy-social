import React from 'react';
import { Link } from 'react-router-dom';
import './style.scss';
import { Menu, ArrowUpward, ArrowDownward, Group, ExpandLess, ExpandMore } from '@material-ui/icons/';
import OrderStatus from '../../../../components/OrderStatus';

export default class CombinedOrderState extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props){
    super(props);
  
    this.state={
      open: false,
      isAdmin: false
    }

    this.handleClick = this.handleClick.bind(this);
    this.showOrderStatus = this.showOrderStatus.bind(this);
  
  }

  showOrderStatus(){
    this.setState({
      open: !this.state.open
    })
  }

  handleClick(){
    this.setState({
      open: !this.state.open
    })
  }

  componentDidMount(){
    if(window.localStorage.getItem('LoggedInUser') && "admin" == window.localStorage.getItem('LoggedInUser')){
      this.setState({
        isAdmin: true
      });
    }
  }

  render() {
    return (
      <div className="combined-order-state-wrapper">
        <div className="combined-order-state-header">
          <div className="order-status-name"> <Group/> Group Order Status:  {this.props.itemsCount} items </div>
          <div className="order-status-count"> 
            {!this.state.open &&
              <ExpandLess onClick={this.showOrderStatus}/>
            }

            {this.state.open &&
              <ExpandMore onClick={this.showOrderStatus}/>
            }
          </div>
        </div>
        <div className="combined-order-state-body">
          {this.state.open &&
              <OrderStatus cartData={this.props.cartData} isAdmin={this.state.isAdmin}/>
          }
        </div>
      </div>
    );
  }
}
