/**
 * The local state selectors
 */

import { createSelector } from 'reselect';

const selectMenu = (state) => state.get('menu');

const makeSelectMenu = () => createSelector(
  selectMenu,
  (menuState) => menuState.getIn('menuData')
);

export {
  makeSelectMenu,
};
