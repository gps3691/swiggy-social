export const LOAD_MENU = 'boilerplate/DetailLayout/LOAD_MENU';
export const LOAD_MENU_SUCCESS = 'boilerplate/DetailLayout/LOAD_MENU_SUCCESS';
export const LOAD_MENU_ERROR = 'boilerplate/DetailLayout/LOAD_MENU_ERROR';

export const UPDATE_CART = 'boilerplate/DetailLayout/UPDATE_CART';
export const UPDATE_CART_SUCCESS = 'boilerplate/DetailLayout/UPDATE_CART_SUCCESS';
export const UPDATE_CART_ERROR = 'boilerplate/DetailLayout/UPDATE_CART_ERROR';
