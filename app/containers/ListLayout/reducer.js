/*
 * ListReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  LOAD_LIST_SUCCESS,
  LOAD_LIST,
  LOAD_LIST_ERROR,
} from './constants';

// The initial state of the ListPage
const initialState = fromJS({
  loading: false,
  error: false,
  listData: []
});

function listReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_LIST:
      return state
        .set('loading', true)
        .set('error', false)
        .setIn('listData', []);
    case LOAD_LIST_SUCCESS:
      return state
        .setIn('listData', action.list)
        .set('loading', false)
    case LOAD_LIST_ERROR:
      return state
        .set('error', action.error)
        .set('loading', false);
    default:
      return state;
  }
}

export default listReducer;