import React from 'react';
import { Link } from 'react-router-dom';
import './style.scss';

export default class SideMenu extends React.Component { 
  constructor(props){
    super(...props);
  
    this.state={
      ...props
    }

    this.handleMenuClick = this.handleMenuClick.bind(this);
  }

  handleMenuClick(){
    this.setState({
      open: !this.state.open
    })
  }

  componentDidMount(){
  }

  componentWillReceiveProps(){
    this.setState({
      open: this.props.open
    })
  }

  render() {
    return (
      <div>
        <div className={"side-menu-overlay " + (this.state.open?'open':"hide")} onClick={this.handleMenuClick}>
        </div>
        <div className= {"side-menu " + (this.state.open?'open':"")}>
            <Link className="menu-item" to="/">
              Home
            </Link>
            <Link className="menu-item" to="/feedback">
              Feedback
            </Link>
            <Link className="menu-item" to="/list">
              List
            </Link>
        </div>
      </div>
    );
  }
}
