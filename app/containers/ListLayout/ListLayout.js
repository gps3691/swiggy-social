import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import Card from './Partials/Card/Card.js';
import FriendSelector from '../../components/FriendSelector';
import './style.scss';
import { Redirect } from 'react-router';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import FriendSelectorModal from '../../components/FriendSelector/FriendSelectorModal.js'


export default class ListLayout extends React.PureComponent { 
  constructor(props){
    super(props);
    this.state = {
      listData: [],
      updatedListData: [],
      selectedResto: null,
      showModal: false
    }
    this.filterList = this.filterList.bind(this);
    this.handleCardSelection = this.handleCardSelection.bind(this);
    this.passRestoToInvite = this.passRestoToInvite.bind(this);
    this.notify = this.notify.bind(this);
  }

  componentDidMount() {
      this.props.loadListData();
  }

  filterList(){
    let items = this.state.listData;
    if(event.target.value){
      items = items.filter(function(item){
        return (item.name.toLowerCase().search(event.target.value.toLowerCase()) !== -1 || item.cusines.toLowerCase().search(event.target.value.toLowerCase()) !== -1);
        });
    }
    this.sortList(items, -1);
    this.setState({updatedListData: items});

  }

  handleCardSelection(e){
    let $elList = document.querySelectorAll('.list-page .Card');

    $elList.forEach((i,v)=> {i.classList.remove('selected')})

    let $el = e.target.closest('.Card');
    
    if($el.classList.contains("selected")){
        $el.classList.remove("selected");
    }
    else{
        $el.classList.add("selected");
    }

    this.setState({
      selectedResto: $el.children.restoId.value
    })
  }

  passRestoToInvite(){
    if(!this.state.selectedResto){
      // console.log('Select Resto First');
      this.notify();
    }
    else{
      this.setState({
        showModal: true
      })
    }
  }

  takeToRestoPage = () =>{
    this.props.history.push('/list/' + this.state.selectedResto)
  }

  sortList(items, order){
    items.sort((a, b) => {
        let keyA = parseFloat(a.rating),
            keyB = parseFloat(b.rating);
        if (keyA < keyB) 
            return -1 * order;
        if (keyA > keyB) 
            return 1 * order;
        return 0;
    });
    this.setState({updatedListData: items});
  }

  componentWillReceiveProps(data){
      this.setState({listData: data.listData, updatedListData: data.listData})       
  }

  notify() {
    toast.error("Please select a restaurant before inviting friend !");
  }

  render() {
    const { loading, error} = this.props;
    let {listData} = this.state;

    return (
      <article>
        <Helmet>
          <title>List Page</title>
        </Helmet>
        <div className="list-page">
          <ToastContainer  autoClose={3000}/>
          <section className="centered">
            <h2>Invite your friends/family member to order for group</h2>
            {loading && <span>Loading...</span>}

            <div className="search-bar">
              <input type="text" placeholder="Search" onChange={this.filterList}/>
            </div>

            {this.state.updatedListData.map(hit =>
                <Card key={hit.name} cardData={hit} handleMethod={this.handleCardSelection}/>
            )}
            <FriendSelector clickMethod={this.passRestoToInvite}/>

            {this.state.showModal &&
              <FriendSelectorModal visible={this.state.showModal} redirMethod={this.takeToRestoPage}/>
            }

          </section>
        </div>
      </article>
    );
  }
}

ListLayout.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  listData: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
  ]),
};
