import { fromJS } from 'immutable';

import {
  LOAD_LIST_SUCCESS,
  LOAD_LIST,
  LOAD_LIST_ERROR,
} from './constants';

const initialState = fromJS({
  loading: false,
  error: false,
  listData: []
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_LIST:
      return state
        .set('loading', true)
        .set('error', false)
        .setIn('listData', []);
    case LOAD_LIST_SUCCESS:
      return state
        .setIn('listData', action.list)
        .set('loading', false)
    case LOAD_LIST_ERROR:
      return state
        .set('error', action.error)
        .set('loading', false);
    default:
      return state;
  }
}

export default appReducer;
