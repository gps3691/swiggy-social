import React from 'react';
import { Helmet } from 'react-helmet';
import './style.scss';

export default class CheckoutLayout extends React.Component {
  shouldComponentUpdate() {
    return false;
  }

  render() {
    return (
      <div className="checkout-page">
        <h1>Checkout Page</h1>
      </div>
    );
  }
}
