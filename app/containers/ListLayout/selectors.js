/**
 * The local state selectors
 */

import { createSelector } from 'reselect';

const selectList = (state) => state.get('list');

const makeSelectList = () => createSelector(
  selectList,
  (listState) => listState.getIn('listData')
);

export {
  makeSelectList,
};
