export const LOAD_LIST = 'boilerplate/App/LOAD_LIST';
export const LOAD_LIST_SUCCESS = 'boilerplate/App/LOAD_LIST_SUCCESS';
export const LOAD_LIST_ERROR = 'boilerplate/App/LOAD_LIST_ERROR';
export const DEFAULT_LOCALE = 'en';
