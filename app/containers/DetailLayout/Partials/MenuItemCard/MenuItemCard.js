import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

export default class MenuItemCard extends React.Component { 
  constructor(props){
    super(...props);

    this.state = {
      count: 0,
      userName: null,
      ...props
    }

    this.handleSelection = this.handleSelection.bind(this);
    this.addToCart = this.addToCart.bind(this);
    this.removeFromCart = this.removeFromCart.bind(this);
    
  }

  componentDidMount(){
    if(window.localStorage.getItem('LoggedInUser')){
      this.setState({
        userName: window.localStorage.getItem('LoggedInUser')
      });
    }
  }

  handleSelection(){
    this.setState({
      selected: !this.state.selected
    })
  }

  addToCart(){
    this.setState((prevState, props) => ({
      count: prevState.count + 1
    }));

    let user;
    if(!this.state.userName){
      user = "admin";
    }
    else{
      user = this.state.userName;
    }
    this.props.handleAddToCart({'itemId': this.state.cardData.productId , 'itemName': this.state.cardData.displayName, 'count': this.state.count, 'addedBy': user})
  }

  removeFromCart(){
    this.setState((prevState, props) => ({
      count: prevState.count - 1
    }));

    let user;
    if(!this.state.userName){
      user = "admin";
    }
    else{
      user = this.state.userName;
    }
    this.props.handleRemoveFromCart({'itemId': this.state.cardData.productId , 'itemName': this.state.cardData.displayName, 'count': this.state.count, 'addedBy': user})
  }

  render() {
    const { cardData, handleMethod } = this.props;
    return (
          <div className={"menu-item-card " + (this.state.selected?"selected":"")} onClick={handleMethod}>
            <input id="restoId-{cardData.id}" type="hidden" value={cardData.id}/>
            <div className="card-image">
              <img src={cardData.img} height="150" width="250"/>
            </div>
            
            <div className="card-title">
              {cardData.displayName}
            </div>

            <div className="footer-wrapper">
              <div className="card-price">
                &#8377; {cardData.price}
              </div>
              {this.state.count == 0 && 
                <div className="menu-add-button-empty" onClick={this.addToCart}>
                  <span> Add </span>
                </div>
              }
              {this.state.count > 0 && 
                <div className="menu-add-button">
                  <button className="subtract" onClick={this.removeFromCart}> – </button>
                  <input className="cart-count" type="text" value={this.state.count} readonly="true"/>
                  <button className="add" onClick={this.addToCart}> + </button>
                </div>
              }
            </div>
            
            {/*<div className="card-order-status">
               <span>Group order has: {this.state.count} No.s </span>
            </div>*/}
          </div>
    );
  }
}

MenuItemCard.propTypes = {
  cardData: PropTypes.oneOfType([
    PropTypes.object,
  ]),
};
