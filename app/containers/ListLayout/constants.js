export const LOAD_LIST = 'boilerplate/ListLayout/LOAD_LIST';
export const LOAD_LIST_SUCCESS = 'boilerplate/ListLayout/LOAD_LIST_SUCCESS';
export const LOAD_LIST_ERROR = 'boilerplate/ListLayout/LOAD_LIST_ERROR';
