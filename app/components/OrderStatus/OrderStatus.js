import React from 'react';
import { Link } from 'react-router-dom';
import './style.scss';
import { Menu } from '@material-ui/icons/';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


export default class OrderStatus extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props){
    super(props);
    this.state={
      cartData: this.props.cartData,
      isAdmin: this.props.isAdmin
    }
    this.handleCheckout = this.handleCheckout.bind(this);

  }


  handleCheckout(){
    if(this.state.cartData && this.state.cartData.length > 0)
      toast.success('Order placed successfully')
    else
      toast.error('Select some items first')
  } 

  render() {
    return (
      <div className="order-status-page">
        <ToastContainer autoclose={3000}/>  
        {!this.state.cartData && 
          <div className="group-order-status-item">
            <span className="group-order-item-name">NO ITEMS ADDED YET</span> 
          </div>
        }
        {this.state.cartData && this.state.cartData.length> 0 && 
            this.state.cartData.map(hit =>
              <div>
              <div className="group-order-status-item">
                <span className="group-order-item-name">{hit.itemName}</span>
                <span className="group-order-item-count">{hit.groupCount}</span> 
              </div>
              
                {hit.userDetails.map(item =>
                  <div className="group-order-item-details">
                    <span className="group-order-item-name">{item.addedBy} has added {item.count} </span>
                  </div>
                )}
              </div>
            )
        }

        {this.state.isAdmin && 
        <div className="checkout-btn-div">
          <button className="checkout-button" onClick={this.handleCheckout}> Checkout </button>
        </div>
        }
      </div>
    );
  }
}
