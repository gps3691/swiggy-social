import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import {
  makeSelectMenu,
} from 'containers/DetailLayout/selectors';

import {
  makeSelectLoading,
  makeSelectError
} from 'containers/App/selectors';

import { loadMenu } from '../DetailLayout/actions';
import reducer from './reducer';
import saga from './saga';
import DetailLayout from './DetailLayout';

const mapDispatchToProps = (dispatch) => ({
  loadMenuData: (evt) => {
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadMenu());
  }
});

const mapStateToProps = createStructuredSelector({
  menuData: makeSelectMenu(),
  loading: makeSelectLoading(),
  error: makeSelectError()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'menu', reducer });
const withSaga = injectSaga({ key: 'menu', saga });

export default compose(withReducer, withSaga, withConnect)(DetailLayout);
export { mapDispatchToProps };
