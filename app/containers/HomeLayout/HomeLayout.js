import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import './HomeLayoutStyles.scss';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default class HomeLayout extends React.Component { // eslint-disable-line react/prefer-stateless-function

  constructor(props){
    super(props);

    this.handleUser = this.handleUser.bind(this);
    this.searchRestaurnt = this.searchRestaurnt.bind(this);
    this.signout = this.signout.bind(this);
    this.state = {
      userName:  null
    }
  }


  componentDidMount(){
    if(window.localStorage.getItem('LoggedInUser')){
      this.setState({
        userName: window.localStorage.getItem('LoggedInUser')
      });
    }
  }

  handleUser = (event) =>{
    let userNameEl = document.querySelector('.user-name');
    if(userNameEl.value.length == 0){
      toast.error('Enter User Name');
    }
    else{
      window.localStorage.setItem('LoggedInUser', userNameEl.value);
      this.setState({
        userName: userNameEl.value
      });
    }
  }

  searchRestaurnt(){
    this.props.history.push('/list');
  }

  signout(){
    window.localStorage.removeItem('LoggedInUser');
    this.setState({
        userName: null
      });
  }
  
  render() {
    const { loading = false, error, repos } = this.props;

    return (
      <article>
        <Helmet>
          <title>Swiggy Social</title>
        </Helmet>
        <div className="home-page">
          <section className="centered">
            <h2> Home Page </h2>
            <ToastContainer  autoClose={3000}/>
            {this.state.userName && 
              <div className="logged-in-case">
                <div> Welcome {this.state.userName}</div>
                <button type="button" className="search-button" onClick={this.searchRestaurnt}>Search Restaurants Near You </button>
                <div>
                  <button type="button" className="signout-button" onClick={this.signout}>Signout</button>
                </div>
              </div>
            }
            {!this.state.userName && 
              <div className="logged-out-case">
                <input type="text" className="user-name" placeHolder="Enter Your Name"/>
                <button type="button" className="submit-button" onClick={this.handleUser}>Submit</button>
              </div>
            }

          </section>
        </div>
      </article>
    );
  }
}
