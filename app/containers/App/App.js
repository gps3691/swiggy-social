import React from 'react';
import { Helmet } from 'react-helmet';
import { Switch, Route } from 'react-router-dom';

import HomeLayout from 'containers/HomeLayout/Loadable';
import ListLayout from 'containers/ListLayout/Loadable';
import DetailLayout from 'containers/DetailLayout/Loadable';
import CheckoutLayout from 'containers/CheckoutLayout/Loadable';
import FeedbackLayout from 'containers/FeedbackLayout/Loadable';

import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Animations from 'components/Animations';
import Header from 'components/Header';
import './style.scss';


const App = () => (
  <div className="app-wrapper">
    <Helmet
      titleTemplate="Swiggy Social App"
      defaultTitle="Swiggy Social App"
    >
      <meta name="description" content="Swiggy Social Ordering Application" />
    </Helmet>
    <Header />
    <Switch>
      <Route exact path="/" component={HomeLayout} />
      <Route exact path="/list" component={ListLayout} />
      <Route exact path="/list/:id" component={DetailLayout} />
      <Route exact path="/checkout" component={CheckoutLayout} />
      <Route path="/anim" component={Animations} />
      <Route path="/feedback" component={FeedbackLayout} />
      <Route path="" component={NotFoundPage} />
    </Switch>
  </div>
);

export default App;
