## Quick start

1. Clone this repository.
2. Go to folder: `cd swiggy-social`
3. Run `yarn` or `npm install` to install dependencies.
4. Run `npm start` to see app at `http://localhost:3000`.
5. For Question 2, visit `http://localhost:3000/feedback` page.

6. Use 'admin' as user name for admin - view;



