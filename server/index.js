const express = require('express');
const logger = require('./util//logger');
const fs = require('fs');
const argv = require('./util/argv');
const port = require('./util//port');
const setup = require('./middlewares/frontendMiddleware');
const { resolve } = require('path');
const path = require('path');
const dataJson = require('../app/Data/data.json');
let cartJson = require('../app/Data/db.json');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json())

//fileService
const writeFile = () => {
   fs.readFile('../app/Data/db.json', JSON.stringify(cartJson) , 'utf-8', (err, data) =>{
       if(err)
          console.log("error in write file :", err);
        console.log(data);

   });
}

// If you need a backend, e.g. an API, add your custom backend-specific middleware here
// app.use('/api', myApi);

// In production we need to pass these values in instead of relying on webpack
setup(app, {
  outputPath: resolve(process.cwd(), 'build'),
  publicPath: '/',
});

// get the intended host and port number, use localhost and port 3000 if not provided
const customHost = argv.host || process.env.HOST;
const host = customHost || null; // Let http.Server use its default IPv6/4 host
const prettyHost = customHost || 'localhost';

app.post('/api/addItem', (req, res) => { 
  //iterate
  var currentItem;
  if(cartJson.Cart && cartJson.Cart.length != 0){
    let itemIndex = cartJson.Cart.findIndex((item)=> {return item.itemId == req.body.itemId})
      if(itemIndex != -1){
          let index = cartJson.Cart[itemIndex].userDetails.findIndex(user => user.addedBy == req.body.addedBy);
          if(index != -1){
              cartJson.Cart[itemIndex].userDetails[index].count+= req.body.count;
              cartJson.Cart[itemIndex].groupCount+= req.body.count;

          }else{

            cartJson.Cart[itemIndex].userDetails.push({
                  addedBy:req.body.addedBy,
                  count: req.body.count
            })
            cartJson.Cart[itemIndex].groupCount+= req.body.count;
            
          }
      }else{
        //if no item exists
          console.log(req.body.itemId);
          cartJson.Cart.push({
              itemId: req.body.itemId,
              itemName : req.body.itemName,
              groupCount: req.body.count,
              userDetails : [
                {
                  addedBy:req.body.addedBy,
                  count: req.body.count
                }
              ]
         })
      }
  }else{
      //if no cart exists/empty
      console.log(req.body.itemId);
      console.log(req.body);
      cartJson.Cart.push({
              itemId: req.body.itemId,
              itemName : req.body.itemName,
              groupCount: req.body.count,
              userDetails : [
                {
                  addedBy:req.body.addedBy,
                  count: req.body.count
                }
              ]
         })
  }

  fs.writeFileSync(path.join(__dirname, '../app/Data/db.json'), JSON.stringify(cartJson) , 'utf-8');
  
  res.send({
    cart:cartJson.Cart,
    message:"Cart updated Successfully :)"
  })
});

app.post('/api/placeOrder', (req, res) => {
  const orderId = Math.floor((Math.random() * 82866) + 1);
  res.send({
    orderId,
    message: "Order Placed"
  })
});

app.post('/api/getMenu', (req, res) => {
  let data = [];
  for (const key of Object.keys(dataJson.data)) {
    data.push(dataJson.data[key]);
  }
  console.log(data);
  res.send({
    data: data
  });
});

// Start your app.
app.listen(port, host, (err) => {
  if (err) {
    return logger.error(err.message);
  }
  logger.appStarted(port, prettyHost);
});


