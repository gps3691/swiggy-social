import React from 'react';
import { Link } from 'react-router-dom';
import './style.scss';
import Fab from '@material-ui/core/Fab';
import { Add} from '@material-ui/icons';

export default class FriendSelector extends React.Component {
  constructor(props){
    super(props);

    this.state={
      show: true
    }
  }

  render() {
    return (
      <div className="friend-selector-wrapper" onClick={this.props.clickMethod}>
        <div className="friend-selector">
          <Fab color='primary'>
            <Add/>
          </Fab>
          <span className="fs-text">Invite Friends</span>
        </div>

      </div>
    );
  }
}


