import React from 'react';
import { Link } from 'react-router-dom';
import './style.scss';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default class Animations extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props){
    super(props);

    this.checkLines = this.checkLines.bind(this);
  
    this.intervalsWrapper = this.intervalsWrapper.bind(this);
    this.arrangeDivs = this.arrangeDivs.bind(this);
    this.notifyCharLimit = this.notifyCharLimit.bind(this);
    this.notifyFeedback = this.notifyFeedback.bind(this);

    this.state ={
      showRect: true,
      showDM: false,
      showArc: false
    }

    this.counter = 0
  }

  notifyCharLimit() {
    toast.error("Maximum character limit reached !");
  }

  notifyFeedback() {
    toast.error("Fill some feedback text first !");
  }

  componentDidMount(){
  }

  intervalsWrapper(){
    var textDiv = document.querySelector('.textDiv');
    if(textDiv.value.length == 0 || textDiv.value == " "){
      //alert('toast fill feedback first');
      this.notifyFeedback();
      return;
    }

    var a = document.querySelectorAll('.test')
    a.forEach((i) => i.classList.remove('hide'))

    var timer = setInterval( this.arrangeDivs, 3000);
  }

  arrangeDivs(){
    let cnt = this.counter;
    if(cnt % 3 == 0){
      //hide dm and circle, show rect
      var a = document.querySelectorAll('.rect')
      a.forEach((i) => i.classList.remove('hide'))

      var a = document.querySelectorAll('.dm')
      a.forEach((i) => i.classList.add('hide'))

      var a = document.querySelectorAll('.circle')
      a.forEach((i) => i.classList.add('hide'))

    }

    if(cnt %3 == 1){
      //hide rect and circle, show dm
      var a = document.querySelectorAll('.rect')
      a.forEach((i) => i.classList.add('hide'))

      var a = document.querySelectorAll('.dm')
      a.forEach((i) => i.classList.remove('hide'))

      var a = document.querySelectorAll('.circle')
      a.forEach((i) => i.classList.add('hide'))
    }

    if(cnt %3 == 2){
      //hide rect and dm, show circle
      var a = document.querySelectorAll('.rect')
      a.forEach((i) => i.classList.add('hide'))

      var a = document.querySelectorAll('.dm')
      a.forEach((i) => i.classList.add('hide'))

      var a = document.querySelectorAll('.circle')
      a.forEach((i) => i.classList.remove('hide'))

    }
    this.counter +=1; 
  }

  checkLines(e){
    if(e.target.value.length >=45){
      e.preventDefault();
      this.notifyCharLimit();
      return;
    }
  }

  render() {
    return (
      <div className="animation-page">
          <ToastContainer  autoClose={3000}/>
          <svg class="test hide" viewbox="0 0 800 800">
            <path class="key-anim1 rect"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M300 300, 600 300"/>
            <path class="key-anim2 rect"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M600 300, 600 600"/>
            <path class="key-anim3 rect"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M600 600, 300 600"/>
            <path class="key-anim4 rect"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M300 600, 300 300"/>

            <path class="key-anim5 dm hide"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M300 300, 450 150"/>
            <path class="key-anim6 dm hide"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M450 150, 600 300"/>

            <path class="key-anim7 dm hide"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M600 300, 750 450"/>
            <path class="key-anim8 dm hide"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M750 450, 600 600"/>

            <path class="key-anim9 dm hide"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M600 600, 450 750"/>
            <path class="key-anim10 dm hide"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M450 750, 300 600"/>

            <path class="key-anim11 dm hide"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M300 600, 150 450"/>
            <path class="key-anim12 dm hide"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M150 450, 300 300"/>

            <path class="key-anim13 circle hide"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M300,300 Q450,150 600,300"/>
            <path class="key-anim14 circle hide"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M600,300 Q750,450 600,600"/>
            <path class="key-anim15 circle hide"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M600,600 Q450 750 300,600"/>
            <path class="key-anim16 circle hide"
                  fill="none"
                  stroke-width="2px"
                  stroke="rgba(9, 200, 143, .7)"
                  d="M300,600 Q150,450 300,300"/>

            
            <circle r="5" cy="300" cx="300"
                    fill="#09c88f"/>
            <circle r="5" cy="600" cx="300"
                    fill="#09c88f"/>
            <circle r="5" cy="300" cx="600"
                    fill="#09c88f"/>
            <circle r="5" cy="600" cx="600"
                    fill="#09c88f"/>
          </svg>
          <textarea rows="3" cols="30" className="textDiv" onChange={this.checkLines} onKeyPress={this.checkLines}> </textarea>
          <button type="button" className="submitFeedbackBtn" onClick={this.intervalsWrapper}> Submit </button>
      </div>
    );
  }
}