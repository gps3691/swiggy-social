import React, { Component } from 'react';
import Modal from 'react-awesome-modal';
import Close from '@material-ui/icons';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Select from 'react-select';
import  MultiSelectReact  from 'multi-select-react';

const options = [
  { value: 'rahul', label: 'Rahul' },
  { value: 'pankaj', label: 'Pankaj' },
  { value: 'priyanka', label: 'Priyanka' },
  { value: 'deepak', label: 'Deepak' },
  { value: 'nikhil', label: 'nikhil' },
  { value: 'om', label: 'Om' }
];

export default class FriendSelectorModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // visible : false
            friends: [],
            ...props
        }

        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.sendInvites = this.sendInvites.bind(this);
    }

    sendInvites(){
    	if(this.state.friends.length == 0){
    		toast.error('Please select atleast one friend');
    	}
    	else{
    		this.closeModal();
    		toast.success('Invitations Sent!');	
    		setTimeout(this.props.redirMethod, 2000);
    	}
    }


    setFriends(optionsList){
    	if(optionsList){
	    	this.setState({
	    		friends: optionsList
	    	})	
    	}
    }

    openModal() {
        this.setState({
            visible : true
        });
    }

    closeModal() {
        this.setState({
            visible : false
        });
    }

    componentDidMount(){
    	this.setState({
    		visible: this.props.visible
    	})
    }

    optionClicked(){
    	this.setState({
    		friends: []
    	})
    }

    render() {
    	const selectedOptionsStyles = {
            color: "#3c763d",
            backgroundColor: "#dff0d8"
        };

        const optionsListStyles = {
            backgroundColor: "#dff0d8",
            color: "#3c763d"
        };

        return (
            <section className="friend-selector-modal">
            	<ToastContainer autoclose={3000}/>	
                <Modal visible={this.state.visible} width="400" height="400" effect="fadeInUp" onClickAway={() => this.closeModal()}>
                    <div>
                        <div className="modal-title">Select Friend to Invite <span onClick={this.closeModal}> X </span> </div>

                        <div className="select-wrapper">
                        <MultiSelectReact 
						      	options={options}
						      	optionClicked={this.optionClicked.bind(this)}
						       	selectedBadgeClicked={this.setFriends.bind(this)}
  								selectedOptionsStyles={selectedOptionsStyles}
      							optionsListStyles={optionsListStyles}
						 />
						 </div>
                        <button type="button" className="send-invite-btn" onClick={this.sendInvites}> Send Invites </button>
                    </div>
                </Modal>
            </section>
        );
    }
}