import React from 'react';
import PropTypes from 'prop-types';

export default class Card extends React.PureComponent { 
  render() {
    const { cardData } = this.props;
    return (
      <article>
          <div className="Card">
            <div className="CardImage">
              <img src={img1}/>
            </div>
            <div className="CardTitle">
              {card.name}
            </div>
            <div className="CardSubtitle">
              <p>{card.cusines}</p>
            </div>
            <div className="FooterWrapper">
              <div className="CardRating">
                &#x2605; {card.rating}
              </div>
              <div className="CardDelivery">
                {card.deliveryTime} Mins
              </div>
            </div>
          </div>
      </article>
    );
  }
}

Card.propTypes = {
  cardData: PropTypes.oneOfType([
    PropTypes.object,
  ]),
};
