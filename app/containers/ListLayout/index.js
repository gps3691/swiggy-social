import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import {
  makeSelectList,
} from 'containers/ListLayout/selectors';

import {
  makeSelectLoading,
  makeSelectError
} from 'containers/App/selectors';

import { loadList } from '../ListLayout/actions';
import reducer from './reducer';
import saga from './saga';
import ListLayout from './ListLayout';

const mapDispatchToProps = (dispatch) => ({
  loadListData: (evt) => {
    if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadList());
  }
});

const mapStateToProps = createStructuredSelector({
  listData: makeSelectList(),
  loading: makeSelectLoading(),
  error: makeSelectError()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'list', reducer });
const withSaga = injectSaga({ key: 'list', saga });

export default compose(withReducer, withSaga, withConnect)(ListLayout);
export { mapDispatchToProps };
