import { call, put, select, takeLatest } from 'redux-saga/effects';
import { LOAD_MENU } from 'containers/DetailLayout/constants';
import { menuLoaded, menuLoadingError } from 'containers/DetailLayout/actions';
import request from 'utils/request';

export function* getMenu() {
  const requestURL = `http://localhost:3000/api/getMenu`;
  try {
    const resp = yield call(request, requestURL,{
 		 method: 'POST',
	});
	
    yield put(menuLoaded(resp));
  } catch (err) {
    yield put(menuLoadingError(err));
  }
}

export default function* menuData() {
  yield takeLatest(LOAD_MENU,  getMenu);
}
